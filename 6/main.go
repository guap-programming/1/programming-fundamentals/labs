package main

import (
	"errors"
	"fmt"
)

func main() {
	var matrix [][]int

	matrix = append(matrix, []int{-1, -2, 0})
	matrix = append(matrix, []int{3, 4, 0})
	matrix = append(matrix, []int{0, 0, 0})

	fmt.Print("matrix:\n")
	for _, v := range matrix {
		fmt.Printf("%v\n", v)
	}

	positiveRow, err := firstPositiveRow(matrix)
	if err == nil {
		fmt.Printf("\nfirst row with positive element in row: %d\n", positiveRow)
	} else {
		fmt.Print(err.Error(), "\n")
	}

	optimizedMatrix := optimizeMatrix(matrix)

	fmt.Print("\noptimized matrix:\n")
	for _, v := range optimizedMatrix {
		fmt.Printf("%v\n", v)
	}

}

func firstPositiveRow(matrix [][]int) (int, error) {
	for index, row := range matrix {
		for _, value := range row {
			if value > 0 {
				return index, nil
			}
		}
	}

	return 0, errors.New("no positive rows")
}

func transpose(matrix [][]int) [][]int {
	xl := len(matrix[0])
	yl := len(matrix)
	result := make([][]int, xl)
	for i := range result {
		result[i] = make([]int, yl)
	}
	for i := 0; i < xl; i++ {
		for j := 0; j < yl; j++ {
			result[i][j] = matrix[j][i]
		}
	}
	return result
}

func removeEmptyRow(matrix [][]int) [][]int {
	var resultMatrix [][]int
	for rowIndex, row := range matrix {
		var isNotEmpty bool
		for _, colValue := range row {
			if colValue != 0 {
				isNotEmpty = true
			}
		}
		if isNotEmpty {
			resultMatrix = append(resultMatrix, matrix[rowIndex])
		}

	}

	return resultMatrix
}

func optimizeMatrix(matrix [][]int) [][]int {
	return transpose(removeEmptyRow(transpose(removeEmptyRow(matrix))))
}
