package main

import (
	"fmt"
	"math"
	"math/rand"
	"sort"
	"time"
)

func main() {
	n := 10
	rand.Seed(time.Now().UTC().UnixNano())
	floats := make([]float64, 0, n)
	for i := 0; i < n; i++ {
		floats = append(floats, math.Round(rand.NormFloat64()*100)/100)
	}
	fmt.Printf("floats: %v\n", floats)

	var maxAbsIndex int

	var firstPositiveFinded bool
	var sumAfterFirstPositive float64
	for k, v := range floats {
		if math.Abs(v) > math.Abs(floats[maxAbsIndex]) {
			maxAbsIndex = k
		}

		if v > 0 {
			firstPositiveFinded = true
		}

		if firstPositiveFinded {
			sumAfterFirstPositive += math.Abs(v)
		}
	}
	fmt.Printf("max abs is: %.2f\n", floats[maxAbsIndex])
	fmt.Printf("abs sum after first positive: %.2f\n", sumAfterFirstPositive)

	var a = 1.0
	var b = 2.0
	sort.Slice(floats, func(i, j int) bool {
		return floats[i] > math.Min(a, b) && floats[i] < math.Max(a, b)
	})

	fmt.Printf("sorted: %v\n", floats)
}
