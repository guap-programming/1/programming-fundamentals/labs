#include <iostream>
#include <cmath>
#include "locale.h"

using namespace std;

int main() {
    setlocale(LC_ALL, "rus");
    double alpha = 0;
    cout << "Введите угол: ";
    cin >> alpha;
    double z1 = 0;
    double z2 = 0;
    z1 = ((sin(4 * alpha)) / (1 + cos(4 * alpha))) * ((sin(2 * alpha)) / (1 + cos(2 * alpha)));
//ctg(alpha) = cos(alpha)/sin(alpha)
    if (alpha != 0)
        z2 = cos((3 / 2) * 3.14 - alpha) / sin((3 / 2) * 3.14 - alpha);
    cout << "z1: " << z1 << " z2: " << z2 << endl;
}