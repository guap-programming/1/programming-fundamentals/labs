#include <iostream>
#include <cmath>
#include "locale.h"

using namespace std;

int main() {
    setlocale(LC_ALL, "rus");
    double Xbegin, Xend, dx;
    Xbegin = Xend = dx = 0;
    int a, b, c;
    a = b = c = 0;
    cout << "Введите Х нач: ";
    cin >> Xbegin;
    cout << "Введите Х кон: ";
    cin >> Xend;
    cout << " dx:  Введите шаг ";
    cin >> dx;
    cout << "Введите a:";
    cin >> a;
    cout << "Введите b:";
    cin >> b;
    cout << "Введите c:";
    cin >> c;
    if (dx == 0.0) {
        cout << "  При введенных условиях создается бесконечный цикл"
             << endl;
        return 0;
    }
    double F = 0;
    for (double x = Xbegin; x <= Xend; x += dx) {
        if (x < 0.6 && (b + c) != 0) {
            cout << " 1 Решение по варианту" << endl;
            F = (a * pow(x, 3)) + pow(b, 2) + c;
        } else if (x > 0.6 && b + c == 0) {
            cout << " 2 Решение по варианту" << endl;
            if ((x - c) == 0.0) F = 0;
            else F = (x - a) / (x - c);
        } else {
            cout << " 3 Решение по варианту" << endl;
            if (c == 0 && a != 0)
                F = (x / a);
            else if (c != 0 && a == 0)
                F = (x / c);
            else if (c == 0 && a == 0)
                F = 0;
            else
                F = (x / c) + (x / a);
        }
        cout << "При x = " << x << " F = " << F << endl;
    }
    return 0;
}